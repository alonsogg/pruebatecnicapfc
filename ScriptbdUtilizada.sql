USE [master]
GO
/****** Object:  Database [PracticaTecnicaDB]    Script Date: 23/3/2021 16:53:15 ******/
CREATE DATABASE [PracticaTecnicaDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PracticaTecnicaDB', FILENAME = N'D:\sqlServer\MSSQL15.MSSQLSERVER\MSSQL\DATA\PracticaTecnicaDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PracticaTecnicaDB_log', FILENAME = N'D:\sqlServer\MSSQL15.MSSQLSERVER\MSSQL\DATA\PracticaTecnicaDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [PracticaTecnicaDB] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PracticaTecnicaDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PracticaTecnicaDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PracticaTecnicaDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PracticaTecnicaDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PracticaTecnicaDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PracticaTecnicaDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET RECOVERY FULL 
GO
ALTER DATABASE [PracticaTecnicaDB] SET  MULTI_USER 
GO
ALTER DATABASE [PracticaTecnicaDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PracticaTecnicaDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PracticaTecnicaDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PracticaTecnicaDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PracticaTecnicaDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'PracticaTecnicaDB', N'ON'
GO
ALTER DATABASE [PracticaTecnicaDB] SET QUERY_STORE = OFF
GO
USE [PracticaTecnicaDB]
GO
/****** Object:  Table [dbo].[Departamento]    Script Date: 23/3/2021 16:53:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Departamento](
	[idDepartamento] [int] NOT NULL,
	[idJefe] [int] NULL,
	[nombre] [varchar](100) NULL,
	[telefono] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[idDepartamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 23/3/2021 16:53:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empleado](
	[idEmpleado] [int] NOT NULL,
	[nombre] [varchar](50) NULL,
	[apellido] [varchar](100) NULL,
	[telefono] [varchar](20) NULL,
	[correo] [varchar](50) NULL,
	[idJefe] [int] NULL,
	[idDepartamento] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[idEmpleado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Marca]    Script Date: 23/3/2021 16:53:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Marca](
	[idMarca] [int] IDENTITY(1,1) NOT NULL,
	[idEmpleado] [int] NULL,
	[fecha] [date] NULL,
	[horaEntrada] [time](7) NULL,
	[horaSalida] [time](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[idMarca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Departamento] ([idDepartamento], [idJefe], [nombre], [telefono]) VALUES (1, 1, N'TI', N'27151000')
INSERT [dbo].[Departamento] ([idDepartamento], [idJefe], [nombre], [telefono]) VALUES (2, 2, N'Recursos humanos', N'26131000')
GO
INSERT [dbo].[Empleado] ([idEmpleado], [nombre], [apellido], [telefono], [correo], [idJefe], [idDepartamento]) VALUES (1, N'Alonso', N'Gamboa Godínez', N'85117526', N'alonso@gmail.com', 1, 1)
INSERT [dbo].[Empleado] ([idEmpleado], [nombre], [apellido], [telefono], [correo], [idJefe], [idDepartamento]) VALUES (2, N'Paola', N'Cerdaz Murillo', N'85117526', N'alonso@gmail.com', 1, 2)
INSERT [dbo].[Empleado] ([idEmpleado], [nombre], [apellido], [telefono], [correo], [idJefe], [idDepartamento]) VALUES (3, N'Johan', N'Valverde Murillo', N'85117526', N'Johan@gmail.com', 1, 1)
INSERT [dbo].[Empleado] ([idEmpleado], [nombre], [apellido], [telefono], [correo], [idJefe], [idDepartamento]) VALUES (4, N'Oscar', N'Parajeles Corrales', N'85117526', N'Oscar@gmail.com', 2, 2)
INSERT [dbo].[Empleado] ([idEmpleado], [nombre], [apellido], [telefono], [correo], [idJefe], [idDepartamento]) VALUES (5, N'Fabiana', N'Bolaños Muñoz', N'85117526', N'Fabiana@gmail.com', 1, 1)
INSERT [dbo].[Empleado] ([idEmpleado], [nombre], [apellido], [telefono], [correo], [idJefe], [idDepartamento]) VALUES (6, N'Antony', N'Alvarez Navarro', N'85117526', N'Antony@gmail.com', 2, 2)
INSERT [dbo].[Empleado] ([idEmpleado], [nombre], [apellido], [telefono], [correo], [idJefe], [idDepartamento]) VALUES (7, N'Jesus', N'Soto Rojas', N'85117526', N'Jesus@gmail.com', 1, 1)
INSERT [dbo].[Empleado] ([idEmpleado], [nombre], [apellido], [telefono], [correo], [idJefe], [idDepartamento]) VALUES (8, N'María', N'Cordero Aguilar', N'85117526', N'María@gmail.com', 2, 2)
INSERT [dbo].[Empleado] ([idEmpleado], [nombre], [apellido], [telefono], [correo], [idJefe], [idDepartamento]) VALUES (9, N'Luisa', N'Nuñez Vargas', N'85117526', N'Luisa@gmail.com', 1, 1)
INSERT [dbo].[Empleado] ([idEmpleado], [nombre], [apellido], [telefono], [correo], [idJefe], [idDepartamento]) VALUES (10, N'Mariana', N'Pérez Jiménez', N'85117526', N'Mariana@gmail.com', 2, 2)
GO
ALTER TABLE [dbo].[Departamento]  WITH CHECK ADD FOREIGN KEY([idJefe])
REFERENCES [dbo].[Empleado] ([idEmpleado])
GO
ALTER TABLE [dbo].[Empleado]  WITH CHECK ADD FOREIGN KEY([idJefe])
REFERENCES [dbo].[Empleado] ([idEmpleado])
GO
ALTER TABLE [dbo].[Marca]  WITH CHECK ADD FOREIGN KEY([idEmpleado])
REFERENCES [dbo].[Empleado] ([idEmpleado])
GO
/****** Object:  StoredProcedure [dbo].[EliminarDetalle]    Script Date: 23/3/2021 16:53:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alonso Gamboa
-- Create date: 22-03-2021
-- Description:	EliminarDetalle
-- =============================================
CREATE PROCEDURE [dbo].[EliminarDetalle]
	@idMarca int
AS
BEGIN
	DElETE Marca 
	WHERE idMarca=@idMarca
END
GO
/****** Object:  StoredProcedure [dbo].[ModificarDetalle]    Script Date: 23/3/2021 16:53:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alonso Gamboa
-- Create date: 22-03-2021
-- Description:	ModificarDetalle
-- =============================================
CREATE PROCEDURE [dbo].[ModificarDetalle]
	@idMarca int, 
	@fecha date,
	@horaEntrada time,
	@horaSalida time
AS
BEGIN
	UPDATE Marca 
	SET fecha=@fecha,
		horaEntrada=@horaEntrada,
		horaSalida=@horaSalida
	WHERE idEmpleado=@idMarca
END
GO
/****** Object:  StoredProcedure [dbo].[ModificarEmpleado]    Script Date: 23/3/2021 16:53:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ModificarEmpleado]
	@idEmpleado int, 
	@nombre varchar(100),
	@apellido varchar(100),
	@telefono varchar(100),
	@correo varchar(100)
AS
BEGIN
	UPDATE Empleado 
	SET nombre=@nombre,
		apellido=@apellido,
		telefono=@telefono,
		correo=@correo
	WHERE idEmpleado=@idEmpleado
END
GO
USE [master]
GO
ALTER DATABASE [PracticaTecnicaDB] SET  READ_WRITE 
GO
