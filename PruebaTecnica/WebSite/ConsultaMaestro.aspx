﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConsultaMaestro.aspx.cs" Inherits="WebSite.ConsultaMaestro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        <h1>Empleados</h1>
        </div>

        <asp:Button ID="buttonPresionar" runat="server" Text="Consulta Empleados" OnClick="buttonPresionar_Click" />
        <asp:TextBox ID="txtIdEmpleado" runat="server" ToolTip="idEmpleado"></asp:TextBox>
        <asp:Button ID="btnConsultaDeta" runat="server" Text="Consulta Detalle" OnClick="btnConsultaDeta_Click" />
        <asp:Button ID="btnModificar" runat="server" Text="Modificar empleado" OnClick="btnModificar_Click" />
        <div>
        <h2>Lista de Empleados</h2>
        </div>
    </form>
</body>
</html>
