﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using System.Web.UI.HtmlControls;

namespace WebSite
{
    public partial class ConsultaMaestro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
 
        }

        protected void buttonPresionar_Click(object sender, EventArgs e)
        {
            cargarEmpleados();   
        }

        protected void btnConsultaDeta_Click(object sender, EventArgs e)
        {
            Response.Redirect("ConsultaDetalle.aspx?idEmpleado="+txtIdEmpleado.Text);          
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            Response.Redirect("ModificacionMaestro.aspx");
        }

        protected void cargarEmpleados()
        {
            ServiceReference.WebServicePagSoapClient webSerivice = new ServiceReference.WebServicePagSoapClient();
            int cant = webSerivice.ObtenerDetalle().Length;
            //Label[] listaLabels = new Label[cant];
            //Button[] listaBotones = new Button[cant];
            int count = 0;
            while(count<cant)
            {
                HtmlGenericControl myDiv = new HtmlGenericControl("div");
                form1.Controls.Add(myDiv);

                
                Label label = new Label();
                label.Text ="Nombre:"+ webSerivice.ObtenerEmpleados()[count].nombre
                    +"  Apellido:" + webSerivice.ObtenerEmpleados()[count].apellido
                    +"  Telefono:" + webSerivice.ObtenerEmpleados()[count].telefono
                    + "  Correo:" + webSerivice.ObtenerEmpleados()[count].correo; 
                myDiv.Controls.Add(label);

                count++;
            }
            HtmlGenericControl myDivBtn = new HtmlGenericControl("div");
            form1.Controls.Add(myDivBtn);
        }
    }
}