﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSite
{
    public partial class ConsultaDetalle : System.Web.UI.Page
    {
        int idEmpleado = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["idEmpleado"]==null)return;
                ServiceReference.WebServicePagSoapClient webService = new ServiceReference.WebServicePagSoapClient();
                setIdEmpleado(int.Parse(Request.QueryString["idEmpleado"].ToString()));
                idEmpleado = getIdEmpleado();
                this.txtNombre.Text= webService.ObtenerEmpleado(getIdEmpleado()).nombre;
                this.txtApellido.Text = webService.ObtenerEmpleado(getIdEmpleado()).apellido;
                this.txtTelefono.Text = webService.ObtenerEmpleado(getIdEmpleado()).telefono;
                this.txtCorreo.Text = webService.ObtenerEmpleado(getIdEmpleado()).correo;
            }
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("ConsultaMaestro.aspx");
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            
                if (getIdEmpleado() != null)
                {
                    ServiceReference.WebServicePagSoapClient webService = new ServiceReference.WebServicePagSoapClient();
                    WebSite.ServiceReference.Empleado emp = new WebSite.ServiceReference.Empleado();
                    emp.nombre = txtNombre.Text;
                    emp.apellido = txtApellido.Text;
                    emp.telefono = txtTelefono.Text;
                    emp.correo = txtCorreo.Text;
                    webService.ModificarEmpleado(emp, int.Parse(Request.QueryString["idEmpleado"].ToString()));
                }
        }

        public void setIdEmpleado(int id)
        {
            idEmpleado=id;
        }

        public int getIdEmpleado()
        {
            return idEmpleado;
        }
    }
}