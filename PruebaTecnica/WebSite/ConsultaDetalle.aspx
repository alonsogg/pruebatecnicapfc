﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConsultaDetalle.aspx.cs" Inherits="WebSite.ConsultaDetalle" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 50%;
            height: 26px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
         <h1>Modificar Empleados</h1>
       <table style="width:100%">
           <tr>
               <td class="10%">Nombre:</td>
               <td class="90%">
                   <asp:TextBox ID="txtNombre" runat="server" Width="257px"></asp:TextBox>
               </td>
           </tr>
           <tr>
               <td class="10%">Apellido:</td>
               <td class="90%">
                   <asp:TextBox ID="txtApellido" runat="server" Width="257px"></asp:TextBox>
               </td>
           </tr>
           <tr>
               <td style="width:10%">Telefono:</td>
               <td style="width:90%">
                   <asp:TextBox ID="txtTelefono" runat="server" Width="256px"></asp:TextBox>
               </td>
           </tr>
           <tr>
               <td class="10%">Correo:</td>
               <td class="90%">
                   <asp:TextBox ID="txtCorreo" runat="server" Width="254px"></asp:TextBox>
               </td>
           </tr>
       </table>
        <asp:Button ID="btnVolver" runat="server" Text="Volver" OnClick="btnVolver_Click" />
        <asp:Button ID="btnModificar" runat="server" Text="Modificar" OnClick="btnModificar_Click" />
    </form>
</body>
</html>
