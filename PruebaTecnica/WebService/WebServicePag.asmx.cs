﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Services;
using System.Web;
using AccessData;
using Model;

namespace WebService
{
    /// <summary>
    /// Descripción breve de WebServicePag
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServicePag : System.Web.Services.WebService
    {
        [Serializable]
        public class EmpleadoA
        {
            public string nombre="";
            public string apellido = "";
            public string telefono = "";
            public string correo = "";
        }

        [WebMethod]
        public string HelloWorld()
        {
            return "Hola a todos";
        }

        [WebMethod]
        public EmpleadoA[] ObtenerEmpleados()
        {

            DetalleAD accessData = new DetalleAD();
            List<Empleado> lista = accessData.ObtenerEmpleados();
            EmpleadoA[] list = new EmpleadoA[lista.Count()];
            int count = 0;
            foreach (Empleado emp in lista)
            {
                list[count] = new EmpleadoA();
                list[count].nombre = emp.nombre;
                list[count].apellido = emp.apellido;
                list[count].telefono = emp.telefono;
                list[count].correo = emp.correo;
                count++;
            }
            return list;
        }

        [WebMethod]
        public EmpleadoA ObtenerEmpleado(int idEmpleado)
        {

            DetalleAD accessData = new DetalleAD();
            Empleado emp= accessData.ObtenerEmpleado(idEmpleado);
            EmpleadoA empA = new EmpleadoA();
            empA.nombre = emp.nombre;
            empA.apellido = emp.apellido;
            empA.telefono = emp.telefono;
            empA.correo = emp.correo;
            
            return empA;
        }

        [WebMethod]
        public EmpleadoA[] ObtenerDetalle()
        {
            DetalleAD accessData = new DetalleAD();
            List<Empleado> lista = accessData.ObtenerEmpleados();
            EmpleadoA[] list =new EmpleadoA[lista.Count()];
            int count = 0;
            foreach (Empleado emp in lista)
            {
                list[count] = new EmpleadoA();
                list[count].nombre = emp.nombre;
                count++;
            }
            return list;
        }

        [WebMethod]
        public string Aconexion()
        {
            string p = "";
            try
            {
                // Data Source = 192.168.10.13; Initial Catalog = HUREMSDB_DEV; User id = appadmin - desa; Password = AppAdmin2o2o!;
                using (SqlConnection connection = new SqlConnection("Data Source=.;Initial Catalog=PracticaTecnicaDB;Integrated Security=True"))
                {
                    SqlCommand command = new SqlCommand("Select * from Empleado", connection);
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            p += reader["nombre"].ToString()+" "+ reader["apellido"].ToString();
                        }
                    }
                    command.Connection.Close();
                }
            }
            catch(Exception ex)
            {
               
            }
            return p;
        }

        [WebMethod]
        public int ModificarEmpleado(Empleado emp, int idEmpleado)
        {
            DetalleAD accessData = new DetalleAD();
            return accessData.ModificarEmpleado(emp, idEmpleado);
        }

        [WebMethod]
        public string EliminarDetalle()
        {
            return "Hola a todos";
        }
    }
}
