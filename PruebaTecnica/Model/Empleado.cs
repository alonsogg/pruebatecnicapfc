﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Empleado : IEmpleado
    {
        public string nombre { set; get; }
        public string apellido { set; get; }
        public string telefono { set; get; }
        public string correo { set; get; }
        public string departamento { set; get; }
        public string jefe { set; get; }
    }
}
