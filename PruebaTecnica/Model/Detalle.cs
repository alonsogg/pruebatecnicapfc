﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Detalle
    {
        public int idMarca { set; get; }
        public int idEmpleado { set; get; }
        public string fecha { set; get; }
        public string horaEntrada { set; get; }
        public string horaSalida { set; get; }
    }
}
