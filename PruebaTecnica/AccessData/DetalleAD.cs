﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Model;
using System.Data.SqlClient;

namespace AccessData
{
    public class DetalleAD
    {
        private readonly string conexion = "Data Source=.;Initial Catalog=PracticaTecnicaDB;Integrated Security=True";
        //private readonly string conexion = ConfigurationManager.ConnectionStrings["PracticaTecnicaDB"].ConnectionString;

        private Empleado MapEmpleado(SqlDataReader reader)
        {
            return new Empleado()
            {
                nombre = reader["nombre"].ToString(),
                apellido = reader["apellido"].ToString(),
                telefono = reader["telefono"].ToString(),
                correo = reader["correo"].ToString(),
                departamento = reader["idDepartamento"].ToString(),
                jefe = reader["idJefe"].ToString()
            };
        }

        public List<Empleado> ObtenerEmpleados()
        {
            var response = new List<Empleado>();
            try
            {

                var sql = new SqlConnection(conexion);
                var cmd = new SqlCommand("Select * from Empleado", sql);
                //cmd.CommandType = System.Data.CommandType.StoredProcedure;
                sql.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            response.Add(MapEmpleado(reader));
                        }
                    }
               
            }
            catch (Exception ex)
            {
                var mensaje = ex.Message + " " + ex.InnerException;
            }



            return response;
        }

        public Empleado ObtenerEmpleado(int idEmpleado)
        {
            var response = new Empleado();
            try
            {

                var sql = new SqlConnection(conexion);
                var cmd = new SqlCommand("Select * from Empleado where idEmpleado="+idEmpleado, sql);
                sql.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        response=MapEmpleado(reader);
                    }
                }

            }
            catch (Exception ex)
            {
                var mensaje = ex.Message + " " + ex.InnerException;
            }

            return response;
        }

        private Detalle MapDetalle(SqlDataReader reader)
        {
            return new Detalle()
            {
                idMarca = (int)reader["idMarca"],
                idEmpleado = (int)reader["idEmpleado"],
                fecha = reader["fecha"].ToString(),
                horaEntrada = reader["horaEntrada"].ToString(),
                horaSalida = reader["horaSalida"].ToString()
            };
        }


        public async Task<List<Detalle>> ObtenerDetalle(string idEmpleado)
        {
            var response = new List<Detalle>();
            try
            {
                var sql = new SqlConnection(conexion);
                var cmd = new SqlCommand("Select * from Marca where idEmpleado="+idEmpleado, sql);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                await sql.OpenAsync();

                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        response.Add(MapDetalle(reader));
                    }
                }
            }
            catch (Exception ex)
            {
                var mensaje = ex.Message + " " + ex.InnerException;
            }


            return response;
        }

        public int ModificarEmpleado(Empleado dataEmpleado, int idEmpleado)
        {
            int response = 0;

            try
            {
                var sql = new SqlConnection(conexion);
                {
                    var cmd = new SqlCommand("ModificarEmpleado", sql);
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@idEmpleado", idEmpleado));
                        cmd.Parameters.Add(new SqlParameter("@nombre", dataEmpleado.nombre));
                        cmd.Parameters.Add(new SqlParameter("@apellido", dataEmpleado.apellido));
                        cmd.Parameters.Add(new SqlParameter("@telefono", dataEmpleado.telefono));
                        cmd.Parameters.Add(new SqlParameter("@correo", dataEmpleado.correo));

                        sql.Open();
                        response = cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.InnerException);
            }

            return response;

        }

        public int ModificarDetalle(Detalle dataDetalle)
        {
            int response = 0;

            try
            {
                var sql = new SqlConnection(conexion);
                {
                    var cmd = new SqlCommand("ModificarDetalle", sql);
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@idMarca", dataDetalle.idMarca));
                        cmd.Parameters.Add(new SqlParameter("@fecha", dataDetalle.fecha));
                        cmd.Parameters.Add(new SqlParameter("@horaEntrada", dataDetalle.horaEntrada));
                        cmd.Parameters.Add(new SqlParameter("@horaSalida", dataDetalle.horaSalida));
                    
                        sql.Open();
                        response = cmd.ExecuteNonQuery();

                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.InnerException);
            }

            return response;

        }

        public int EliminarDetalle(Detalle dataDetalle)
        {
            int response = 0;

            try
            {
                var sql = new SqlConnection(conexion);
                {
                    var cmd = new SqlCommand("EliminarDetalle", sql);
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@idMarca", dataDetalle.idMarca));
                        
                        sql.Open();
                        response = cmd.ExecuteNonQuery();

                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.InnerException);
            }

            return response;

        }
    }
}
